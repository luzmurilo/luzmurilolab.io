module.exports = {
  pathPrefix: `/novo-portfolio`,
  siteMetadata: {
    title: `Murilo Luz Stucki`,
    description: `My portfolio showing the projects I've worked on.`,
    author: `@luzmurilo`,
  },
  plugins: ["gatsby-plugin-image", "gatsby-plugin-react-helmet", "gatsby-plugin-sharp", "gatsby-transformer-sharp", {
    resolve: 'gatsby-source-filesystem',
    options: {
      "name": "images",
      "path": "./src/images/"
    },
    __key: "images"
  },
  {
    resolve: `gatsby-plugin-manifest`,
    options: {
      name: `luzmurilo-portfolio`,
      short_name: `luzmurilo`,
      start_url: `/`,
      background_color: `#343457`,
      theme_color: `#343457`,
      display: `minimal-ui`,
      icon: `src/images/pixel-portrait.png`, // This path is relative to the root of the site.
    },
  },
  {
    resolve: "gatsby-plugin-react-svg",
    options: {
      rule: {
        include: /images/
      }
    }
  }
  ]
};