import * as React from "react"
import { StaticImage } from "gatsby-plugin-image"

import { Layout } from "../components/layout"
import Seo from "../components/seo"
import { Landing } from "../components/landing"
import { ProjectCard } from "../components/project-card"
import { CallToAction } from "../components/call-to-action"
import { Skills } from "../components/skills"
import Gallery from "../components/gallery"


const IndexPage = () => {
  return (
    <Layout>
      <Seo title="Portfolio" />
      
      <Landing />
      
      <Skills />

      <h1>My Games</h1>

      <ProjectCard>
        <div className="card-content">
            <div className="card-image">
              <iframe width="700" height="400" title="aster video" className="desktop-only"
              src="https://www.youtube.com/embed/hcM00nBz1a8">
              </iframe> 
              <iframe width="300" height="200" title="aster video" className="mobile-only"
              src="https://www.youtube.com/embed/hcM00nBz1a8">
              </iframe> 
            </div>
        </div>
        <div className="card-content">
            <h1 className="card-title">Aster's Probation</h1>
            <p className="card-text">
              Game currently in development.
            </p>
            <p className="card-text">
              This project is a 2D metroidvania game I’m developing solo.  
            </p>
            <p className="card-text">
              It’s currently in the concept phase as I implement and test new ideas, and flesh out the design.
            </p>
            <p className="card-text">
              The video above show the most basic functionalities for movement and combat.
            </p>
            <div className="card-links-container justify-center">
              <a className="card-link" href="https://gitlab.com/luzmurilo/project-aster">
                See the Code!
              </a>
            </div>
        </div>
      </ProjectCard>

      <ProjectCard>
        <div className="card-content">
            <div className="card-image">
              <StaticImage 
              src="../images/constellation.png" 
              alt="constellation screenshot" 
              />
            </div>
        </div>
        <div className="card-content">
            <h1 className="card-title">Constellation</h1>
            <p className="card-text">
              A game made for the Lost Relic GameJam 2022.
            </p>
            <p className="card-text">
              The theme of the Jam was ‘Connection’, so I my idea was to make the theme present in the core mechanic of my game. 
            </p>
            <p className="card-text">
              In this game the goal is to make connections between the ‘Stars’ to form the right constellation. The player is able to shoot colored ‘Planets’ that orbit the Star it collides with, and if another Star has a planet of the same color, a connection is made.
            </p>
            <p className="card-text">
              I developed this game from scratch in a week. All aspects, except the music, were made solo.
            </p>
            <div className="card-links-container">
              <a className="card-link" href="https://muriloluz.itch.io/constellation">
                Play Here!
              </a>
              <a className="card-link" href="https://gitlab.com/luzmurilo/moonshooter">
                See the Code!
              </a>
            </div>
        </div>
      </ProjectCard>

      <ProjectCard>
        <div className="card-content">
            <div className="card-image">
              <StaticImage 
              src="../images/light-guardian.png" 
              alt="light guardian thumbnail" 
              />
            </div>
        </div>
        <div className="card-content">
            <h1 className="card-title">Light Guardian</h1>
            <p className="card-text">
              A game prototype developed using Unity3D.
            </p>
            <p className="card-text">
              The intention behind its design was to be able to prove my understanding of multiple aspects of game development, as well as my coding knowledge. 
            </p>
            <p className="card-text">
              The player goal is to stop the creatures of the night from reaching the town, and the way to do that is by keeping the lanterns in the city entrance always lit.
            </p>
            <p className="card-text">
              I did all of the design and coding by myself, and used free 3D models from the Unity Asset Store.
            </p>
            <div className="card-links-container">
              <a className="card-link" href="https://play.unity.com/mg/other/webgl-builds-172573">
                Play Here!
              </a>
              <a className="card-link" href="https://gitlab.com/luzmurilo/light-guardian-unity">
                See the Code!
              </a>
            </div>
        </div>
      </ProjectCard>
      
      <CallToAction />

      <Gallery />

    </Layout>
  )
}

export default IndexPage
