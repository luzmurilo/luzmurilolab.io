import React from "react"
import { StaticImage } from "gatsby-plugin-image"

export function LandingImage() {
  return <StaticImage 
    src="../images/keyboard.jpg" 
    alt="keyboard" 
    />
}