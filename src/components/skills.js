import React from "react"

import "../styles/skills.css"

export function Skills() {
    return <div className="skills-wrapper">
        <h1 className="skills-title">
            Relevant Skills
        </h1>
        <div className="skills-content">
            <div className="color-box">
                <h3>Unity</h3>
                <p>I started learning Unity in 2018 at college, and early 2022 I completed the Junior Programmer course provided by Unity and since then I've been working on solo projects.</p>
            </div>
            <div className="color-box">
                <h3>C#</h3>
                <p>I learned how to use C# for Unity and for ASP.NET. I use this language almost daily and I'm always learning how to use it better in my projects.</p>
            </div>
            <div className="color-box">
                <h3>JavaScript</h3>
                <p>JavaScript is a language I used a lot during my graduation, even in my Final Project I built an application using JavaScript and Node.js. I also have experience in React and Gatsby.js.</p>
            </div>
            <div className="color-box">
                <h3>HTML and CSS</h3>
                <p>I have experience using HTML, CSS and Sass for professional and personal projects such as this portfolio site.</p>
            </div>
            <div className="color-box">
                <h3>Design</h3>
                <p>I gathered experiences as a UI/UX designer in my internship, and as a Game Designer in college and in game jams I've participated.</p>
            </div>
            <div className="color-box">
                <h3>Photoshop and Digital Illustration</h3>
                <p>I do digital illustration for some of my games, and as a hobby. For that I have learned how to use Adobe Photoshop.</p>
            </div>
        </div>
    </div>
}