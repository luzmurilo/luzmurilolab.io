import React from "react"
import { graphql, useStaticQuery } from "gatsby"
import { GatsbyImage, getImage } from "gatsby-plugin-image"

import "../styles/gallery.css"

import Anya from "../images/gallery/anya.mp4"
import Dog from "../images/gallery/dog.mp4"
import Tanjiro from "../images/gallery/tanjiro.mp4"
import Chester from "../images/gallery/chester.mp4"

function Gallery() {

    const images = useStaticQuery(
        graphql`
        query
        {
            allFile (filter: {extension: {regex: "/(png)|(jpg)|(jpeg)/"}, relativeDirectory: {eq: "gallery"}})
            {
                nodes
                {
                    childImageSharp
                    {
                        gatsbyImageData(width: 500)
                    }
                    name
                }
            }
        }
        `
    )

    return <div className="gallery-wrapper">
        <h1 className="gallery-title">
            Pixel Art
        </h1>
        <div className="gallery-image-solo">
                <video muted controls loop width={600} className="video-border desktop-only" alt="Tanjiro Fire Attack">
                    <source src={Tanjiro} type="video/mp4" />
                </video>
                <video muted autoPlay controls loop width={300} className="video-border mobile-only" alt="Tanjiro Fire Attack">
                    <source src={Tanjiro} type="video/mp4" />
                </video>
        </div>

        <div className="video-grid">
            <div className="gallery-video">
                <video muted controls loop height={284} className="video-border desktop-only" alt="Dog wagging it's tail">
                    <source src={Dog} type="video/mp4" />
                </video>
                <video muted autoPlay controls loop height={100} className="video-border mobile-only" alt="Dog wagging it's tail">
                    <source src={Dog} type="video/mp4" />
                </video>
            </div>

            <div className="gallery-video">
                <video muted controls loop height={284} className="video-border desktop-only" alt="Anya forger animation">
                    <source src={Anya} type="video/mp4" />
                </video>
                <video muted autoPlay controls loop height={100} className="video-border mobile-only" alt="Anya forger animation">
                    <source src={Anya} type="video/mp4" />
                </video>
            </div>

            <div className="gallery-video">
                <video muted controls loop width={284} className="video-border desktop-only">
                    <source src={Chester} type="video/mp4" />
                </video>
                <video muted autoPlay controls loop width={100} className="video-border mobile-only">
                    <source src={Chester} type="video/mp4" />
                </video>
            </div>
        </div>

        <div className="gallery-grid">

            {images.allFile.nodes.map(node => (
                <div className="gallery-image" key={node.name}>
                    <div className="image-border">
                        <GatsbyImage image={getImage(node)} alt={node.name}/>
                    </div>
                </div>
            ))}
        </div>
    </div>
}

export default Gallery