import React from "react"
import { StaticImage } from "gatsby-plugin-image"

export function Portrait() {
  return <StaticImage 
    src="../images/pixel-portrait.png" 
    alt="portrait" 
    width={80}
    height={80}
    layout="fixed"
    />
}