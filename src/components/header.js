import React from "react"

import "../styles/header.css"

import { Portrait } from "./portrait"

export function Header() {
    return <header className="header">
        <div className="header-content">
            <Portrait />
            <div>
                <h2 className="header-text">
                    Murilo Luz Stucki
                </h2>
                <p className="header-text">
                    <span style={{color: "#b5e48c"}}>Game Developer </span>| <span style={{color: "#d9ed92"}}>Front-end</span>
                </p>
            </div>
            <div className="header-contact">
                <a href="mailto:murilo.stucki@gmail.com?subject=From your Portfolio page" aria-label="email" className="email">murilo.stucki@gmail.com</a>
                <div className="social-media">
                    <a href="https://www.linkedin.com/in/luz-murilo/" className="fa fa-linkedin" aria-label="linkedin"><div/></a>
                    <a href="https://gitlab.com/luzmurilo" className="fa fa-gitlab" aria-label="gitlab"><div/></a>
                </div>
            </div>
        </div>
    </header>
}