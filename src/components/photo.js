import React from "react"
import { StaticImage } from "gatsby-plugin-image"

export function Photo() {
  return <div className="clip-circle">
      <StaticImage 
      src="../images/real-photo.png" 
      alt="photo in São Paulo" 
      width={256}
      height={256}
      layout="fixed"
      />
    </div>
}