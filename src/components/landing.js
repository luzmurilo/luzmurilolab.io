import React from "react"

import "../styles/landing.css"

import { LandingImage } from "./landing-image"
import { Photo } from "./photo"

export function Landing() {
    return <div className="landing-wrapper">
        <div className="landing-background">
            <LandingImage />
        </div>
        <div className="portrait-wrapper">
            <Photo />
        </div>
        <div className="landing-content">
            <div className="landing-text">
                <p>Hello! My name is <span style={{color:"#d9ed92"}}>Murilo Luz</span>, and I am a Computer Engineer and Game Developer from Brazil. Doing everything with passion!</p>
            </div>
        </div>
        <div className="bottom-content">
            <div className="color-box">
                <p>Recently graduated from University of São Paulo, I have experience as a front-end web developer and UI/UX designer.</p>
            </div>
            <div className="color-box">
                <p>I’m always learning new things as I work with the latest technologies on Web and Game Development. Check out my Curriculum for more details!</p>
            </div>
        </div>
    </div>
}